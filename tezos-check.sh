#!/bin/bash

# To be called by a cron job every hours or so.
# Checks the health of a baking node.

set -e

fail=""

if [ -z "$(pgrep tezos-node)" ]; then
    fail="${fail}"$'node is dead\n'
fi

if [ -z "$(pgrep tezos-baker)" ]; then
    fail="${fail}"$'baker is dead\n'
fi

# directory where this script is stored
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ -z "$fail" ] ; then #no point in checking rights if the node is dead
    if ! $DIR/check-slots.sh ; then
        fail="${fail}"$'missed slot\n'
    fi
fi

is_disk_full (){
    used=$(df /dev/vda1 | grep vda | awk '{print substr($5,1,length($5)-1)}')
    if [[ $used > 80 ]]
    then return 0
    else return 1
    fi
}

if is_disk_full ; then
    fail="${fail}"$'disk is full\n'
fi

if [ -n "$fail" ] ; then
    # set up mail aliases in /etc/aliases to receive root mails to your
    # personal inbox
    echo "$fail" | mail -s Alert root@localhost
fi
