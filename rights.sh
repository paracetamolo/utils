#!/bin/bash

set -e

url=localhost:${1:-8732}

tmp=$(curl -s ${url}/chains/main/blocks/head)
level=$(echo $tmp | jq .header.level)
cycle=$(echo $tmp | jq .metadata.level.cycle)

baker_in_wallet=$(cat $HOME/tezos-client/public_key_hashs | jq ' .[] | select(.name == "baker") | .value' | sed 's/"//g')
delegate=${1:-$baker_in_wallet}

echo "Delegate: $delegate"

for i in $(seq 0 1); do

    echo Cycle: $((cycle + $i))

    echo baking:
    curl -s $url'/chains/main/blocks/head/helpers/baking_rights?delegate='$delegate'&max_priority=1&cycle='$((cycle + $i)) | jq -c '.[] | select(.level > '$level') | [.level, .estimated_time]'

    echo endorsing:
    curl -s $url'/chains/main/blocks/head/helpers/endorsing_rights?delegate='$delegate'&cycle='$((cycle + $i)) | jq -c '.[] | select(.level > '$level') | [.level, .estimated_time]'

done
