#!/bin/bash

set -e

DIR=$HOME
client=$DIR/tezos-client

log=$DIR/log
bin=$DIR/bin

if [ -d $log ]; then mv $log ${log}-$(date +%m%d%H%M); fi
mkdir $log

export TEZOS_LOG="client.signer.ledger -> debug"
$bin/tezos-signer -d $client launch socket signer -a 10.0.0.3 &> $log/signer.log &
