#!/bin/bash

# logs number of current peers and number of different peers wrt to
# previous log

# to log every 15 minutes add to crontab:
# */15 * *   *   *   /home/tezos/utils/log-memory.sh

set -e

url=localhost:${1:-8732}

curl -s ${url}/network/connections | jq '.[] | .peer_id' | sort -h > /tmp/tezos-peers-new

size=$(cat /tmp/tezos-peers-new | wc -l)

if [ -f /tmp/tezos-peers ]; then
    common=$(comm -12 /tmp/tezos-peers* | wc -l)
    diff=$(( size - common ))
fi
mv /tmp/tezos-peers-new /tmp/tezos-peers

log=log-p2p

if ! [ -f $log ]; then
    echo "#size diff" > $log
fi
echo $size $diff >> $log
