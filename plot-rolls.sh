#!/bin/bash

# curl -s localhost:8732/chains/main/blocks/head/votes/listings > listings

# cat listings | sed 's/{"pkh"://g' | sed 's/"rolls"://g' | sed 's/},/}/g' | sed 's/"\|\]\|\[//g' | tr '}' '\n' | grep -v "^$" | sort -n -t',' -k 2> listings.csv

cat listings | jq '.[].rolls' | sort -n > rolls

number_bakers=$(wc -l rolls | awk '{print $1}')

# histogram
cat rolls | awk '
BEGIN{bucket=10}{
res[int($1/bucket)]+=1
}END{for (i in res) {printf("%i %i\n", i*bucket, res[i])}}
' > histo

echo "\
input='histo'
set terminal svg
#set terminal dumb
set logscale x
set logscale y
set title 'Distribution of stake for $number_bakers bakers'
set key top left
plot input using 1:2 title 'rolls'
" | gnuplot
#rm -f rolls histo



curl -s https://api.mytezosbaker.com/v1/bakers/ > bakers
cat bakers | jq '.bakers | .[] | .delegation_code , .baker_name' | tr '\n' ',' | sed 's/"//g' | awk -F',' '{for (i=1; i<NF; i++){if((i % 2)==0){printf("%s,\"%s\"\n", $(i-1), $i)}}}' > registry-mytezosbaker


sort -t',' listings.csv > listings.sort
sort -t',' registry-mytezosbaker > registry.sort
join -t',' -a 1 listings.sort registry.sort | sort -t',' -k2 -n > registry-mytezosbaker-rolls
rm -f listings.sort registry.sort


curl -s https://api.baking-bad.org/v1/aliases/tz1gg5bjopPcr9agjamyu9BbXKLibNc2rbAq | jq .name


#compute half of total rolls
awk -F',' '{a+=$2}END{print a/2}' listings.csv
#find how many small guys you need to reach half
awk -F',' 'BEGIN{a=0}{if(a < 37329){a+=$2}else{print NR}}END{print a}' listings.csv
#result: you need 420 delegates to reach half, which is equivalent as the top 13
# if we split by 3 we get that the 1st third is 406, 2nd is 18, 3rd 7


awk -F',' '{if($3=="" && $2 > 100){print $1}}' registry-mytezosbaker-rolls > missing-names

sort -t',' registry-mytezosbaker-rolls > registry-mytezosbaker-rolls.sort
sort -t',' registry-tzstats > registry-tzstats.sort
join -t',' -a 1 registry-mytezosbaker-rolls.sort registry-tzstats.sort | sort -t',' -k2 -n > registry-rolls
rm -f registry-mytezosbaker-rolls.sort registry-tzstats.sort
