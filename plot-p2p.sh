echo "\
input=\"${1}\"
#set terminal svg
set terminal dumb
set key bottom left
set logscale y
#set title ''
set xlabel 'one point every 15 minutes'
#set ylabel '# connections (logscale)'
plot input using (column(0)):1 with lines ls 1 title '# connections', \
     input using (column(0)):2 with lines ls 2 title '# changes'
" | gnuplot
