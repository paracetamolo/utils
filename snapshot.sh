#!/bin/bash

set -e

bin=$HOME/bin
node=$HOME/tezos-node
ip=localhost
rpc=8732

#length=$((4096 * 2))
length=0

header=$(curl localhost:8732/chains/main/blocks/head~$length/header)
hash=$(echo $header | jq -r .hash)
level=$(echo $header | jq .level)

echo $bin/tezos-node snapshot --data-dir=$node --block=$hash export $HOME/snapshot-$level-$hash-$(date +%Y%m%d-%H%M%S).full
