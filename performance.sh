#!/bin/bash

set -e

url=localhost:8732

offset=${1:-0}

cycle=$(curl -s ${url}/chains/main/blocks/head | jq .metadata.level.cycle)

delegate=$(cat $HOME/tezos-client/public_key_hashs | jq ' .[] | select(.name == "baker") | .value' | sed 's/"//g')

levels=$(curl -s $url'/chains/main/blocks/head/helpers/endorsing_rights?delegate='$delegate'&cycle='$((cycle - $offset)) | jq '.[] | .level')

for level in $levels ; do
    ans=$(curl -s ${url}/chains/main/blocks/$((level + 1)) | jq '.operations | .[0] | .[].contents | .[] | select(.metadata.delegate == "'$delegate'")' )

    if [ -z "$ans" ]
    then echo "$level: X"
    else echo "$level: V"
    fi
done
