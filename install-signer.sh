#!/bin/bash

set -e

DIR=$HOME
bin=$DIR/bin

read -p "remove $bin ?"
rm -rf $bin
mkdir -p $bin

cd $DIR/tezos
eval $(opam env)
dune build src/bin_client/main_client.exe src/bin_signer/main_signer.exe
cp _build/default/src/bin_client/main_client.exe tezos-client
cp _build/default/src/bin_signer/main_signer.exe tezos-signer
cp -r tezos-* $bin
git branch -vv | grep ^* > $bin/version
echo >> $bin/version
git log -n1 >> $bin/version
