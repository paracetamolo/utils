#!/bin/bash

set -e

#url=mainnet-node.tzscan.io:8732
url=localhost:${1:-8732}

seconds_per_block() {
    delta_blocks=${1:-100}
    head=$(curl -s ${url}/chains/main/blocks/head | jq .hash | sed 's/"//g')

    end=$(curl -s ${url}/chains/main/blocks/${head} | jq .header.timestamp | sed 's/"//g')
    beg=$(curl -s ${url}/chains/main/blocks/${head}~${delta_blocks} | jq .header.timestamp | sed 's/"//g')

    delta_seconds=$(( $(date --date=${end} +%s) - $(date --date=${beg} +%s) ))

    echo | awk "{print int($delta_seconds / $delta_blocks)}"
}

blocks_per_voting_period=$(curl -s ${url}/chains/main/blocks/head/context/constants | jq .blocks_per_voting_period)
level=$(curl -s ${url}/chains/main/blocks/head | jq .header.level)
seconds_per_block=$(seconds_per_block)

period=$(( $level / $blocks_per_voting_period ))
delta_blocks=$(echo | awk "{print $blocks_per_voting_period - int($level % $blocks_per_voting_period)}")
seconds=$(( $delta_blocks * $seconds_per_block ))
switch_level=$(( $level + $delta_blocks ))

echo "level $level"
echo "seconds_per_block $seconds_per_block"
echo "period $period"

echo -n "Next period starts at level $switch_level on "
date -d "+ $seconds seconds"

switch_level=$(( $switch_level + $blocks_per_voting_period ))
seconds=$(( $blocks_per_voting_period * $seconds_per_block + $seconds ))
echo -n "Next period starts at level $switch_level on "
date -d "+ $seconds seconds"

switch_level=$(( $switch_level + $blocks_per_voting_period ))
seconds=$(( $blocks_per_voting_period * $seconds_per_block + $seconds ))
echo -n "Next period starts at level $switch_level on "
date -d "+ $seconds seconds"

switch_level=$(( $switch_level + $blocks_per_voting_period ))
seconds=$(( $blocks_per_voting_period * $seconds_per_block + $seconds ))
echo -n "Next period starts at level $switch_level on "
date -d "+ $seconds seconds"
