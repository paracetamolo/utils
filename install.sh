#!/bin/bash

set -e

DIR=$HOME
bin=$DIR/bin

read -p "remove $bin ?"
rm -rf $bin
mkdir -p $bin

cd $DIR/tezos
make
cp -r tezos-* $bin
git branch -vv | grep ^* > $bin/version
echo >> $bin/version
git log -n1 >> $bin/version
