#!/bin/bash

# to log every 15 minutes add to crontab:
# */15 * *   *   *   /home/tezos/utils/log-memory.sh

set -e

#url=mainnet-node.tzscan.io:8732
url=localhost:${1:-8732}
log=log-mem

mem () {
    ps -o rss,command ax | grep $1 | grep -v grep | head -1 | cut -f1 -d' '
}

if ! [ -f $log ]; then
    (
        echo "# memory (resident set size from `ps`) in KB"
        echo "#level node baker endorser accuser"
    ) > $log
fi

(
    echo -n "$(curl -s ${url}/chains/main/blocks/head | jq .header.level) "
    for i in tezos-{node,baker,endorser,accuser}*; do
        mem $i
    done | tr '\n' ' '
    echo
) >> $log
