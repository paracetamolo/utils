#!/bin/bash

set -e

url=localhost:8732

baker_in_wallet=$(cat $HOME/tezos-client/public_key_hashs | jq ' .[] | select(.name == "baker") | .value' | sed 's/"//g')
delegate=${1:-$baker_in_wallet}

for i in $(seq 0 9); do

    back=$((4096 * $i))

    tmp=$(curl -s ${url}/chains/main/blocks/head~$back)
    # level=$(echo $tmp | jq .header.level)
    cycle=$(echo $tmp | jq .metadata.level.cycle)

    balances=$(curl -s ${url}/chains/main/blocks/head~$back/context/delegates/$delegate)
    balance=$(echo $balances | jq .balance | sed 's/"//g' | awk '{print $1/1000000}')
    frozen=$(echo $balances | jq .frozen_balance | sed 's/"//g' | awk '{print $1/1000000}')

    echo "$cycle $balance $frozen"

done > /tmp/balances

cat /tmp/balances

echo "\
input=\"/tmp/balances\";
#set terminal svg;
set terminal dumb;
set key bottom left;
#set logscale y;
set yrange [0:4000]
#set title ''
set xlabel 'cycle';
#set ylabel '# connections (logscale)';
plot input using 1:2 with lines ls 1 title 'balance', \
     input using 1:3 with lines ls 2 title 'frozen'
" | gnuplot
