#!/bin/bash

set -e

# set up delegate alice from faucet

DIR=/home/ubuntu
bin=$DIR/bin
rpc=18732 #default for {alpha,zero}net
client=$DIR/tezos-client

for a in faucet/* ; do
    $bin/tezos-client -d $client -P $rpc -w none activate account alice$cnt with $a
    cnt=$((cnt+1))
done

for i in $(seq 1 8); do
    balance=$($bin/tezos-client -d $client -P $rpc get balance for alice$i)
    $bin/tezos-client -d $client -P $rpc -w none transfer $(echo $balance | cut -d'.' -f1) from alice$i to alice
done

$bin/tezos-client -d $client -P $rpc register key alice as delegate
