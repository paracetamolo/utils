#!/bin/bash

# check that all the past endorsements of a baker have actually been included
# in the chain

set -e

url=localhost:8732

tmp=$(curl -s ${url}/chains/main/blocks/head)
current_level=$(echo $tmp | jq .header.level)
cycle=$(echo $tmp | jq .metadata.level.cycle)

last_level=$(cat /tmp/last_level || echo 0)

baker_in_wallet=$(cat $HOME/tezos-client/public_key_hashs | jq ' .[] | select(.name == "baker") | .value' | sed 's/"//g')
delegate=${1:-$baker_in_wallet}

endorsement_levels=$(curl -s $url'/chains/main/blocks/head/helpers/endorsing_rights?delegate='$delegate'&cycle='$cycle | jq -c '.[] | select(.level <= '$current_level') | .level')
if [ "$endorsement_levels" == "null" ]; then endorsement_levels=""; fi

#echo -e "endorsements for delegate $delegate:\n$endorsement_levels"
for level in $endorsement_levels; do

    if [ $level -le $last_level ]; then continue ; fi

    endorsement=$(curl -s $url'/chains/main/blocks/'$((level + 1))'/operations/0' | jq '.[] | select(.contents[0].metadata.delegate == "'$delegate'")')

    if [ -z "$endorsement" ]; then
        echo "missing endorsement $level"
        echo "$current_level" > /tmp/last_level
        exit 1
    fi
done
echo "$current_level" > /tmp/last_level
