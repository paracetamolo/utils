#!/bin/bash

set -e

DIR=$HOME
client=$DIR/tezos-client

net=9732
rpc=8732
ip=10.0.0.1
node=$DIR/tezos-node
log=$DIR/log
bin=$DIR/bin
account=baker

if [ -d $log ]; then mv $log ${log}-$(date +%m%d%H%M); fi
mkdir $log

$bin/tezos-node run --net-addr [::]:$net --rpc-addr localhost --rpc-addr $ip:$rpc --data-dir $node &> $log/node.log &

sleep 10

for proto in 007-PsDELPH1; do
    $bin/tezos-baker-$proto -E $ip:$rpc -d $client run with local node $node $account &> $log/$proto-bakr.log &
    $bin/tezos-endorser-$proto -E $ip:$rpc -d $client run $account &> $log/$proto-endo.log &
#    $bin/tezos-accuser-$proto -E $ip:$rpc -d $client run &> $log/$proto-accu.log &
done
