echo "\
input=\"${1}\"
#set terminal svg
set terminal dumb
set key top left
set logscale y
set title 'memory (resident set size from `ps`) in KB'
#set xlabel 'level'
set ylabel 'memory KB (logscale)'
plot input using 1:2 with lines ls 1 title 'node', \
     input using 1:3 with lines ls 2 title 'baker', \
     input using 1:4 with lines ls 3 title 'endorser', \
     input using 1:5 with lines ls 4 title 'accuser'
" | gnuplot
